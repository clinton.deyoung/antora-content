= Antora & Asciidoc Features

This demo showcases many of the Asciidoc features that do not exist in Markdown that could make documentation more accessible and robust.

== Versioning

As you can see on the left, I have three versions of my documentation on the same page. Antora supports versioning using:

- *Git branches*: This is the most robust since these branches will be editable in the future and are low maintenance.
- *Git tags*: These are similar to Git branches, but we cannot go back and edit them to fix typos once we deploy them.
- *Version folders*: These are editable but require high maintenance.

== All Markdown supported tags

Asciidoc supports all the same formatting as Markdown.

== Blocks

Asciidoc supports different kinds of delimited blocks.

=== Literal blocks

You can use literal blocks to bypass the Asciidoc parser and print text exactly as you type it. You can even add titles to blocks. For example:

.This is how you create headers in Asciidoc
....
= Document Title
== Header 1
=== Header 2
==== Header 3
....

=== Example blocks

.This is an example block
====
As you can see, example blocks are automatically numbered so you can reference them elsewhere in the text.
====

=== Listing blocks

.This is a listing block showing the directory structure of an Antora project.
----
├── docs
│   ├── antora.yml
│   └── modules
│       └── ROOT
│           ├── nav.adoc
│           └── pages
│               ├── asciidoc-demo.adoc
│               └── index.adoc
├── package-lock.json
└── package.json
----

=== Sidebars

.This is a sidebar
****
Sidebars work similarly to example blocks, but they are not numbered and the title is inside the block instead of on top.
****

== Literal monospacing

Markdown doesn't support things like HTML entities, so you have to escape them to keep them from breaking the page. Asciidoc supports literal monospacing, which let's you type code without worrying about having to escape certain characters. For example:

`+<p>Anybody want PB&amp;J?</p>+`

== Adding custom CSS classes

In Asciidoc, you can add custom CSS classes to anything you want. For example,

`+I can add a CSS class to [.red-text]#this text to turn it red#.+`

This would generate:

`+<p>I can add a CSS clas to <span class="red-text">this text to turn it red</span>.+`

If I have time, I'll add custom CSS to do this instead of just explaining it.